﻿using System.Collections.Generic;
using NUnit.Framework;
using ResourcesVerification.DataProviders;
using ResourcesVerification.Model;

namespace ResourcesVerification.Tests
{
    [TestFixture]
    public class DeviceMonitorResourcesTests
    {
        private static readonly TestCasesGenerator TestCasesGenerator = new TestCasesGenerator(new DpResourcesDataProvider());

        [Test, Category("Integration"),
         TestCaseSource(typeof(TestCasesDataSource), nameof(TestCasesDataSource.CreateTestCasesPerKey))]
        public void VerifyResourcesConsistency(Data data)
        {
            ResourcesVerifier.VerifyResources(data, TestCasesGenerator.TestData);
        }

        [Test, Category("Integration"), TestCaseSource(typeof(TestCasesDataSource), nameof(TestCasesDataSource.CreateTestCasesPerTranslation))]
        public void VerifyKeysDuplication(KeyValuePair<string, List<Data>> translation)
        {
            ResourcesVerifier.VerifyKeysDuplication(translation);
        }

        private class TestCasesDataSource
        {
            public static IEnumerable<TestCaseData> CreateTestCasesPerKey()
            {
                var testCases = TestCasesGenerator.GenerateTestCasesPerKey();
                foreach (var testCase in testCases)
                {
                    yield return testCase;
                }
            }

            public static IEnumerable<TestCaseData> CreateTestCasesPerTranslation()
            {
                var testCases = TestCasesGenerator.GenerateTestCasesPerTranslation();
                foreach (var testCase in testCases)
                {
                    yield return testCase;
                }
            }
        }
    }
}