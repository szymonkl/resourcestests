﻿using System.Collections.Generic;
using NUnit.Framework;

namespace ResourcesVerification.Tests
{
    internal class TestCasesSource
    {
        private static TestCasesGenerator _testCasesGenerator;

        public TestCasesSource(TestCasesGenerator testCasesGenerator)
        {
            _testCasesGenerator = testCasesGenerator;
        }

        public IEnumerable<TestCaseData> CreateKeysTestCases()
        {
            var testCases = _testCasesGenerator.PrepareTestCasesPerKey();
            foreach (var testCase in testCases)
            {
                yield return testCase;
            }
        }

        public IEnumerable<TestCaseData> CreateTranslationsTestCases()
        {
            var testCases = _testCasesGenerator.PrepareTestCasesPerTranslation();
            foreach (var testCase in testCases)
            {
                yield return testCase;
            }
        }
    }
}
