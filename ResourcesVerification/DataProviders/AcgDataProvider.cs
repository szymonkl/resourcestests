﻿using System.Collections.Generic;

namespace ResourcesVerification.DataProviders
{
    public class AcgDataProvider : IDataProvider
    {
        private readonly List<string> translationFilesPaths = new List<string>();

        public string BaseFilePath { get; } =
            @"C:\Users\Szymon\Source\Repos\ResourcesVerification\ResourcesVerification.Tests\bin\Debug\ResourcesFiles\AcgResources.resx";

        public List<string> TranslationFilesPaths => Helpers.GetTranslationFilesPaths(BaseFilePath);
    }
}