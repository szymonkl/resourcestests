﻿using System.Collections.Generic;

namespace ResourcesVerification.DataProviders
{
    public interface IDataProvider
    {
        string BaseFilePath { get; }
        List<string> TranslationFilesPaths { get; }
    }
}