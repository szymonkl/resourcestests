﻿using System.Collections.Generic;

namespace ResourcesVerification.DataProviders
{
    public class RfResourcesDataProvider : IDataProvider
    {
        public string BaseFilePath { get; } = @"C:\Users\Szymon\Source\Repos\ResourcesVerification\ResourcesVerification.Tests\bin\Debug\ResourcesFiles\RFResources.resx";
        public List<string> TranslationFilesPaths => Helpers.GetTranslationFilesPaths(BaseFilePath);
    }
}