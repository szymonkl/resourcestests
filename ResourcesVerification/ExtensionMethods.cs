﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ResourcesVerification
{
    internal static class ExtensionMethods
    {
        public static List<TKey> GetDuplicatedKeys<TSource,TKey>(this List<TSource> source, Func<TSource, TKey> selector)
        {
            return source.GroupBy(selector)
                .Where(grp => grp.Count() > 1)
                .Select(grp => grp.Key).ToList();
        }
    }
} 

