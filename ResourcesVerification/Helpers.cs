﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ResourcesVerification
{
    internal class Helpers
    {
        internal static List<string> GetTranslationFilesPaths(string baseFile)
        {
            var directory = Path.GetDirectoryName(baseFile);
            if (directory != null)
            {
                var files = Directory.GetFiles(directory);
                var baseFileName = Path.GetFileNameWithoutExtension(baseFile);
                List<string> matchingTranslationFiles;
                if (baseFileName != null)
                {
                    if (files.ToList().Any())
                    {
                        matchingTranslationFiles = files.ToList().Where(f => Path.GetFileNameWithoutExtension(f).StartsWith(baseFileName)).ToList();
                        if (!matchingTranslationFiles.Any())
                        {
                            throw new Exception("Not found any matching translation file");
                        }
                    }
                    else
                    {
                        throw new IOException("Cannot load any translation files");
                    }
                }
                else
                {
                    throw new IOException("Cannot get base file name");
                }

                return matchingTranslationFiles;
            }

            throw new IOException("Cannot find Resources directory");
        }
    }
}
