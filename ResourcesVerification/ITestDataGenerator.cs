﻿namespace ResourcesVerification
{
    public interface ITestDataGenerator
    {
        TestData LoadTestData();
    }
}