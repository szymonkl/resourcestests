﻿using ResourcesVerification.Model;

namespace ResourcesVerification
{
    internal interface IXmlParser
    {
        Root Parse(string path);
    }
}