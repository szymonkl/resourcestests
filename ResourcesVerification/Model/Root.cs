﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ResourcesVerification.Model
{
    [XmlRoot(ElementName = "root")]
    public class Root
    {
        [XmlElement(ElementName = "data")]
        public List<Data> Data { get; set; }
    }
}