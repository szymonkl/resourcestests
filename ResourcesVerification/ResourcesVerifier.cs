﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ResourcesVerification.Model;

namespace ResourcesVerification
{
    public class ResourcesVerifier
    {
        public static void VerifyResources(Data data, TestData translations)
        {
            var errorMessageBuilder = new StringBuilder();
            foreach (var translation in translations.Translations)
            {
                if (!translation.Value.Exists(i => i.Name.Equals(data.Name)))
                {
                    errorMessageBuilder.AppendLine(translation.Key);
                }
            }

            if (!string.IsNullOrEmpty(errorMessageBuilder.ToString()))
            {
                Assert.Fail($"Found missing translation for the key {data.Name} in the following languages:\n" +
                            errorMessageBuilder);
            }
        }

        public static void VerifyKeysDuplication(KeyValuePair<string, List<Data>> translation)
        {
            var duplicatedKeys = translation.Value.GetDuplicatedKeys(k => k.Name);
            var errorMessageBuilder = new StringBuilder();
            duplicatedKeys.ForEach(k => errorMessageBuilder.AppendLine(k));

            var errorMessage = $"For translation file: {translation.Key} found the following duplicated keys:\n"
                               + errorMessageBuilder;

            CollectionAssert.IsEmpty(duplicatedKeys, errorMessage);
        }
    }
}