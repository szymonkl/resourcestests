﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using ResourcesVerification.DataProviders;

namespace ResourcesVerification
{
    public class TestCasesGenerator
    {
        public TestData TestData => this.testData ?? (this.testData = LoadTestData());
        
        private readonly IDataProvider dataProvider;
        private TestData testData;

        private TestData LoadTestData()
        {
            var testDataCreator = new TestDataGenerator(dataProvider);
            return testDataCreator.LoadTestData();
        }


        public TestCasesGenerator(IDataProvider dataProvider)
        {
            this.dataProvider = dataProvider;
        }

        public IEnumerable<TestCaseData> GenerateTestCasesPerKey()
        {
            var testCaseData = TestData.Base.Select(r => new TestCaseData(r).SetName(r.Name));

            foreach (var data in testCaseData)
            {
                yield return data;
            }
        }

        public IEnumerable<TestCaseData> GenerateTestCasesPerTranslation()
        {
            var translations = TestData.Translations;
            translations.Add("Base Resources", TestData.Base);

            var testCaseData = translations.Select(t => new TestCaseData(t).SetName(t.Key));
            foreach (var testCase in testCaseData)
            {
                yield return testCase;
            }
        }
    }
}