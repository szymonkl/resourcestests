﻿using System.Collections.Generic;
using ResourcesVerification.Model;

namespace ResourcesVerification
{
    public class TestData
    {
        public List<Data> Base { get; set; }
        public Dictionary<string, List<Data>> Translations { get; set; }
    }
}
