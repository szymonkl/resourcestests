﻿using System;
using System.Collections.Generic;
using System.IO;
using ResourcesVerification.DataProviders;
using ResourcesVerification.Model;

namespace ResourcesVerification
{
    public class TestDataGenerator : ITestDataGenerator
    {
        private readonly IDataProvider provider;
        private readonly XmlParser parser;
        

        public TestDataGenerator(IDataProvider provider)
        {
            this.provider = provider;
            this.parser = new XmlParser();
        }

        public TestData LoadTestData()
        {
            var testData = new TestData
            {
                Base = ParseBaseFile(),
                Translations = ParseTranslationsFiles()
            };
            return testData;
        }

        private List<Data> ParseBaseFile()
        {
            var baseFileParsed = parser.Parse(provider.BaseFilePath);
            return baseFileParsed.Data;
        }

        private Dictionary<string, List<Data>> ParseTranslationsFiles()
        {
            var translations = new Dictionary<string, List<Data>>();
            foreach (var path in provider.TranslationFilesPaths)
            {
                var translationParsed = parser.Parse(path);
                var fileName = Path.GetFileName(path);
                if (fileName != null)
                {
                    if (!translations.ContainsKey(fileName))
                    {
                        translations.Add(fileName, translationParsed.Data);
                    }
                    else
                    {
                        throw new NotSupportedException(
                            $"There are many translations files {fileName} in the translations directory");
                    }
                }
                else
                {
                    throw new IOException($"Cannot get file for the path: {path}");
                }
            }

            return translations;
        }
    }
}
