﻿using System.Xml;
using System.Xml.Serialization;
using ResourcesVerification.Model;

namespace ResourcesVerification
{
    internal class XmlParser : IXmlParser
    {
        public Root Parse(string path)
        {
            var serializer = new XmlSerializer(typeof(Root));
            Root root;
            using (var xmlReader = XmlReader.Create(path))
            {
                root = (Root)serializer.Deserialize(xmlReader);
            }

            return root;

        }
    }
}
